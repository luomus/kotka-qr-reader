export const environment = {
  production: true,
  start: 'https://kotka.luomus.fi/tools/qr-reader',
  viewers: [
    {id: 'viewer', url: 'https://kotka.luomus.fi/view?uri=%id%', label: 'Kotka viewer'},
    {id: 'edit', url: 'https://kotka.luomus.fi/specimens/edit?uri=%id%', label: 'Kotka edit'},
    {id: 'species_fi', url: 'https://laji.fi/en/view?uri=%id%', label: 'Laji.fi viewer (en)'},
    {id: 'laji_fi', url: 'https://laji.fi/view?uri=%id%', label: 'Laji.fi viewer (fi)'},
    {id: 'accessions', url: 'https://kotka.luomus.fi/accessions?uri=%id%', match: '\\b(P\\.[0-9]+)$'},
    {id: 'collection', url: 'https://kotka.luomus.fi/collections/edit?uri=%id%', match: '\\b(HR\\.[0-9]+)$'},
    {id: 'dataset', url: 'https://kotka.luomus.fi/datasets/edit?uri=%id%', match: '\\b(GX\\.[0-9]+)$'},
    {id: 'transaction', url: 'https://kotka.luomus.fi/transactions/edit?uri=%id%', match: '\\b(HRA\\.[0-9]+)$'},
    {id: 'organization', url: 'https://kotka.luomus.fi/organizations/edit?uri=%id%', match: '\\b(MOS\\.[0-9]+)$'},
  ],
};
