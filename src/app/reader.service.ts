import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

export interface IViewer {
  id: string;
  url: string;
  label?: string;
  match?: string;
}

interface IState {
  active: IViewer;
}

let _state: IState = {
  active: environment.viewers[0]
};

@Injectable({
  providedIn: 'root'
})
export class ReaderService {

  private store  = new BehaviorSubject<IState>(_state);
  readonly state$ = this.store.asObservable();

  readonly active$ = this.state$.pipe(map(state => state.active), distinctUntilChanged());

  getVisibleViewers(): IViewer[] {
    return environment.viewers.filter((view: IViewer) => typeof view.match === 'undefined');
  }

  getUrl(id: string): string {
    let url = _state.active.url;
    environment.viewers.forEach(viewer => {
      if (!viewer.match) {
        return;
      }
      const re = new RegExp(viewer.match);
      if (re.exec(id)) {
        url = viewer.url;
      }
    });
    return url.replace('%id%', id);
  }

  setActiveViewer(view: IViewer) {
    this.updateState({..._state, active: view});
  }

  private updateState(state: IState) {
    this.store.next(_state = state);
  }
}
