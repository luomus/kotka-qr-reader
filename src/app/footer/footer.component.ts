import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {ScannerComponent} from '../scanner/scanner.component';
import { IViewer, ReaderService } from '../reader.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @Input() viewer: IViewer;
  @Input() id: string;

  @Output() viewerChange = new EventEmitter<IViewer>();
  @Output() idChange = new EventEmitter<string>();
  views: IViewer[] = [];
  active$: Observable<IViewer>;

  constructor(
    public dialog: MatDialog,
    private readerService: ReaderService
  ) { }

  ngOnInit() {
    this.active$ = this.readerService.active$;
    this.views = this.readerService.getVisibleViewers();
  }

  onChange(event) {
    this.idChange.emit(event.target.value);
  }

  openCamera() {
    const dialogRef = this.dialog.open(ScannerComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.idChange.emit(result || this.id);
    });
  }

  moveTo(delta: number) {
    const parts = this.id.split('.');
    const lastIdx = parts.length - 1;
    parts[lastIdx] = '' + Math.max(Number(parts[lastIdx]) + delta, 0);
    this.idChange.emit(parts.join('.'));
  }
}
