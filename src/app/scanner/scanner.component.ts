import {Component, OnInit, ViewChild} from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import {LocalStorage} from 'ngx-store';
import {ZXingScannerComponent} from '@zxing/ngx-scanner';
import BarcodeFormat from '@zxing/library/esm5/core/BarcodeFormat';

@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.scss']
})
export class ScannerComponent implements OnInit {

  @ViewChild('scanner', {static: true})
  scanner: ZXingScannerComponent;

  multi = false;
  camera: MediaDeviceInfo;
  formats = [BarcodeFormat.QR_CODE, BarcodeFormat.CODE_39];
  @LocalStorage() selectedCamera: string;
  @LocalStorage() availableCameras: string[];

  constructor(public dialogRef: MatDialogRef<ScannerComponent>) { }

  ngOnInit() {
  }

  camerasFoundHandler(cameras) {
    this.availableCameras = cameras.map(cam => cam.deviceId);
    let idx = this.availableCameras.indexOf(this.selectedCamera);
    if (idx === -1) {
      idx = 0;
    }
    this.camera = cameras[idx];
    this.selectedCamera = this.camera.deviceId;
    this.multi = cameras.length > 1;
  }

  scanSuccessHandler($event) {
    this.dialogRef.close($event);
  }

  switchCamera() {
    let idx = this.availableCameras.indexOf(this.selectedCamera) + 1;
    if (!this.availableCameras[idx]) {
      idx = 0;
    }
    this.selectedCamera = this.availableCameras[idx];
    this.camera = this.scanner.getDeviceById(this.selectedCamera);
  }
}
