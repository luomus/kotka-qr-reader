import {Component, Input, OnInit} from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { environment } from '../../environments/environment';
import { IViewer, ReaderService } from '../reader.service';

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html'
})
export class ViewerComponent implements OnInit {

  _id: string;
  _viewer: IViewer;
  _url: SafeResourceUrl;

  viewerUrls: {[key: string]: string} = {
    'accessions': 'https://kotka.luomus.fi/accessions?uri=%url%',
    'collection': 'https://kotka.luomus.fi/collections/edit?uri=%url%',
    'dataset': 'https://kotka.luomus.fi/datasets/edit?uri=%url%',
    'transaction': 'https://kotka.luomus.fi/transactions/edit?uri=%url%',
    'organization': 'https://kotka.luomus.fi/organizations/edit?uri=%url%',
    'edit': 'https://kotka.luomus.fi/specimens/edit?uri=%url%',
    'viewer': 'https://kotka.luomus.fi/view?uri=%url%',
    'laji': 'https://laji.fi/view?uri=%url%'
  };

  constructor(
    private sanitizer: DomSanitizer,
    private readerService: ReaderService
  ) { }

  ngOnInit() {
    this._url = this.sanitizer.bypassSecurityTrustResourceUrl(environment.start);
  }

  @Input()
  set id(id: string) {
    this._id = id;
    this.updateUrl();
  }

  @Input()
  set viewer(viewer: IViewer) {
    this._viewer = viewer;
    this.updateUrl();
  }

  private updateUrl() {
    if (!this._id || !this._viewer) {
      return;
    }
    this._url = this.sanitizer.bypassSecurityTrustResourceUrl(this.readerService.getUrl(this._id));
  }

  private getUrl(id: string) {
    const viewer = this._viewer.id;
    const short = id.replace('http://tun.fi/', '');
/*
    if (short.indexOf('HRA.') === 0) {
      viewer = 'transaction';
      id = 'http://tun.fi/' + short;
    } else if (short.indexOf('GX.') === 0) {
      viewer = 'dataset';
      id = 'http://tun.fi/' + short;
    } else if (short.indexOf('HR.') === 0) {
      viewer = isEdit ? 'collection' : 'viewer';
      id = 'http://tun.fi/' + short;
    } else if (short.indexOf('MOS.') === 0) {
      viewer = 'organization';
      id = 'http://tun.fi/' + short;
    } else if (short.indexOf('P.') === 0) {
      viewer = 'accessions';
      id = 'http://tun.fi/' + short;
    }
*/
    return this.viewerUrls[viewer].replace('%id%', id);
  }

}
