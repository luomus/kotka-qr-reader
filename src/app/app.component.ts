import {Component, OnInit} from '@angular/core';
import {SwUpdate} from '@angular/service-worker';
import { IViewer, ReaderService } from './reader.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  id = '';
  viewer$: Observable<IViewer>;
  toolbarVisible = true;

  constructor(
    private swUpdate: SwUpdate,
    private readerService: ReaderService
  ) {}

  ngOnInit(): void {
    this.viewer$ = this.readerService.active$;
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        if (confirm('New version available. Load New Version?')) {
          window.location.reload();
        }
      });
    }
  }

  setViewer(viewer: IViewer) {
    this.readerService.setActiveViewer(viewer);
  }

  toggleToolbar() {
    this.toolbarVisible = !this.toolbarVisible;
  }

  setId(id: string) {
    this.id = id;
  }
}
